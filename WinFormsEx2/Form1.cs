﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace WinFormsEx2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string src_path = Environment.CurrentDirectory + "\\src";
            string filepath = src_path+"\\1.svg";
                 

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filepath);
            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);
            string s1 = sw.ToString();
            richTextBox1.Text = s1;
            string xml = Regex.Replace(s1, @"s", "");

            XDocument doc = XDocument.Parse(xml);
            string result = "";
            foreach (XElement element in doc.Descendants("rect"))
            {
                result += element;
            }


            string result_path = src_path + "\\2.svg";
            if (File.Exists(result_path))
            {
                File.Delete(result_path);
            }

            using (FileStream fs = File.Create(result_path))
            {
                Byte[] txt = new UTF8Encoding(true).GetBytes("<svg viewBox='0 0 300 300' xmlns = 'http://www.w3.org/2000/svg'>"+result+"</svg>");
                fs.Write(txt, 0, txt.Length);
            }

            if (File.Exists(filepath))
            {
                System.Diagnostics.Process.Start(result_path);
            }

        }

    }
}
